import { HomeComponent} from '../components/home.component';
import { NavbarComponent } from '../components/navbar.component';
import { DashboardComponent } from '../components/dashboard.component';
import { ModuleWithProviders } from '@angular/core';
import { Route , RouterModule } from '@angular/router' ;
const routes: Route[] = [


    {
        path : 'home',
        component : HomeComponent,
        data: { title: 'Inicio' }
    },
    { 
       path: '', 
        redirectTo: 'home', 
        pathMatch: 'full' 
    },
    
    {
        path : 'navbar',
        component : NavbarComponent
    },

    {
        path : 'dashboard',
        component : DashboardComponent
    }



   
];
export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);